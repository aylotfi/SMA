import java.util.ArrayList;

public class Grid {

    private String Board[][]= new String[10][10];

    public  String[][] getBoard()
    {
        return Board;
    }

    public void setBoard(String[][] board) {
        Board = board;
    }

    public  Grid()
    {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Board[i][j] = ".";
            }
        }
    }


    public void afficher()
    {
        for (int i = 0; i < 10; i++) {
            System.out.print("|");
            for (int j = 0; j < 10; j++) {
                System.out.print(" " +Board[i][j]+" ");;
            }
            System.out.println("|");
        }
    }

}
